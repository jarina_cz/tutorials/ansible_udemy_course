# Ansible Udemy Course Files

## LAB Environment used

firewall - pfsense (local PC inaccessible, internet accessible, inbound limited)

DC - Windows Server 2012 R2 (DNS, DHCP, AD)

control server - master.jarvis.com - ubuntu 18.04

elastic cluster - elastic[1:3].jarvis.com - ubuntu 18.04

graylog cluster - graylog1.jarvis.com - ubuntu 18.04

centos server - centos1.jarvis.com - CentOS 7

webserver - server1.jarvis.com - Windows Server 2016 R2


## NOTE
All passwords are used in local virtual LAB that is inaccessible from outside...